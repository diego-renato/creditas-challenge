# Creditas challenge
No presente `.md` é apresentado algúns dos resultados obtidos do EDA, data preparation, data modeling assim como a avaliação
do modelo escolhido para a solução do problema apresentado por Creditas.
 
 Os scripts que apresentam o código e um análise mais completo pode-se encontrar na pasta  `notebooks`. Para executar os 
 scripts é seguir o ordem, além disso, os scripts *01* e *02* precisam dos dados brutos que pode-se encontrar na pasta dados, 
 ja para o script de modelamiento de dados, os dados necessarios encontram-se na pasta saidas.
 
 Assim, dividiremos em 4 seções que estas são:
## Conteúdo
- [1. Resultados do EDA](#1-resultados-do-EDA)
- [2. Preparação dos dados](#2-preparação-dos-dados)
- [3. Modelagem de dados](#3-modelagem-de-dados)
- [4. Pos análise](#4-pos-análise)

# 1. Resultados do EDA
O conjunto de dados inicial apresenta uma dimensão de (35021, 32). Como o nosso objetivo é de predecir a probabilidade 
que um cliente tem de ser enviado para análise de crédito dado que ele foi pré-aprovado para o empréstimo 
com garantia de automóvel, foi filtrado o conjunto de dados em que só as pessoas que foram pré-aprovados para o empréstimo.
Assim, o conjunto a utilizar-se tem uma dimensão de (14999, 31).

O conjunto de dados apresenta dados perdidos, muitas variáveis categóricas com muitos níveis e a nossa variável resposta 
é do tipo dummy.

<div align="center">
    <img width="450" src="imagenes/sent_to_analysis_pie.png" />
    <p>Figura 1.1: Proporção de clientes que foram enviados para o análise.</p>
</div>
<br>
<br>
<br>

Começou a analisar as variáveis do tipo contínua dos qual apenas 7 são desse tipo. Alguns pontos importantes foram:

- A variável collateral_debt está perdida um 35%.
- Tem-se uma concentração de clientes com uma idade entre 30 a 46 anos.
- Tem-se pontos atípicos na idade, pode-se observar que uma pessoa tem 115 anos.
- As variáveis monthly_income, collateral_value, loan_amount, collateral_debt e monthly_payment tem um comportamento muito assimétrico, sendo que tem uma maior concentração de valores perto de zero.
- Para a variável auto_year tem uma maior concentração de carros entre 2008 e 2013.
- Não existe correlações significativas entre as variáveis do tipo contínuas.
 <div align="center">
    <img width="450" src="imagenes/continuous_features_log.png" />
    <p>Figura 1.2: Variáveis com muita assimetría em escala log +1.</p>
</div>
<br>
<br>
<br>

Logo procedeu-se estudar as variáveis do tipo discreta. Decidiu-se particionar entre as variáveis que tem mais de 50
níveis e as que tem menos de 50. Para as variáveis que tem mais de 50 categorías procedeu se a tratar os strings para obter
insights já que tinha-se níveis como o mesmo significado já seja por uma ruim codificação ou natureza dos dados.
Logo, foi utilizado a lei de Pareto em que o 20% dos níveis explicam o 80% da variabilidde total da variável. 


Top 5 cidades           |  top 5 marcas de carros         |  top 5 razões  
:-------------------------:|:-------------------------:|:-------------------------:
<img width="750" src="imagenes/city_top_5.png" /> | <img width="750" src="imagenes/brand_top_5.png" /> | <img width="750" src="imagenes/purpose_top_5.png" /> | 


<div align="center"> 
<p>Figure 1.3: Top 5 níveis de acordo a variável.</p>
</div>

Assim, temo que:
- Para a variável city, São Paolo e Rio de Janeiro representam o 80% da variabilidade total das cidades.
- Para a variável auto_brand, o modelo Fiat representa o 80% da variação total.
- Para a variável informed_purpose os níveis dividas e without reasons representam o 80% da variabilidade total.

Por último para as variáveis que apresentam menos de 50 níveis foi realizado um estudo para avaliar como se comporta nossa
 variável resposta. Um resultado muito importante foi que quando tem-se informação sobre o estado da pessoa, um 91.8% foi enviado para
 o análise.
 
 As concluções gerais para o nosso EDA foram:

- Um 21.8% de pessoas foram enviadas para o análise dado que ele foi pré-aprovado.
- Para as variaveis monthly_income, collateral_value, loan_amount, collateral_debt e monthly_payment podemos utilizar a transformação do tipo logaritmo para obter valores simétricos e que os valores atípicos sejam menos possivelmente influentes.
- As cidades escolhidas para o nosso futuro análise serão São Paolo e Rio de Janeiro ja que representão o 80% da variação total.
- Para a variável auto_brand, o modelo Fiat representa o 80% da variação total.
- Para a variável informed_purpose os níveis dividas e without reasons representam o 80% da variabilidade total.
- Para a variável state será escolhido o estado de SP.
- Para a variável landing_page_product agrupar se a pessoa fez mediante AutoFin ou AutoRefinancing.
- Criar uma nova variável em que se uma pessoa colocou seu estado civil (marital_status).
- Criar uma variável para especificar se tem-se informação sobre collateral_debt.
- Não é sugerido utilizar as variáveis dishonored_checks, expired_debts e protests já que apresentão pouca variabilidade.

# 2. Preparação dos dados

De acordo ao análise exploratória dos dados, podemos obter insights das variáveis. Assim para a nossa preparação de dados,
utilozou-se as ideias apresentadas nas concluções gerais da seção anterior. Antes de partir a preparação de dados, decidiu-se
particionar os dados em um conjunto de treinamento e um conjunto de test. Assim as operações feitas para o conjunto de treinamento foi feito
para o conjunto de test evitando um possível viés. 

1. Decidiu-se não incluir as variáveis "dishonored_checks", "expired_debts", "protests", "loan_term","auto_model","landing_page"
                    ,"zip_code" e "utm_term".
2. Para as variáveis "monthly_income", "collateral_value", "loan_amount", "collateral_debt" e "monthly_payment", decidiu-se utilizar 
transformar os dados em escala logaritmica + 1.

3. Logo, foi tratado as variáveis do tipo discreto, em que foi utilizado a lei de Pareto para reduzir os níveis não relevantes.

5. Para algumas variáveis perdidas, foi criado novas variáveis indicando se a observação está perdida.

4. Para as variáveis com dados perdidos, foi feito uma imputação mediante KNN.

# 3. Modelagem de dados

Foi utilizado autoML(auto machine learning) mediante h2o, do qual, modelos como MLG, random forest e XGBoost 
foram testados e otimizados mediante CV(5 kfold). Também foi utilizado o algoritmo LightGBM, muito conhecido na atualidade e
com um tempo de convergência menor. Para o algoritmo LightGBM foi realizado a optimização dos hyperparâmetro mediante o algoritmo
 differencial evolution e utilizando CV(5 kfold). O treinamento para os modelos em autoML indicou que o modelo XGBoost deu bons resultados  
 obtendo um AUC_cv de 0.807733, já para o LightGBM deu de 0.81248.
 
 Logo, foi testado no conjunto de test para avaliar o desempenho de ambos algoritmos em um conjunto de dados não observado.
 Assim, a seguinte grafica apresenta o resultado para 3 métricas utilizadas para avaliar: AUC, logloss e f1 score.
 
 Utilizaremos o f1 score já que os dados não estão totalmente balanceados. O f1 score indica a media armonica entre a precisão
 e o recall do modelo. Um valor proximo a um significa que o modelo consegui diferenciar bem as classes. O logloss é uma métrica que por não diz nada sobre o ajuste, mas é muito útilo para comparar.
 <div align="center">
    <img width="450" src="imagenes/metrics_evaluation.png" />
    <p>Figura 3.1: Desempenho dos modelos no conjunto de treinamento e de test.</p>
</div>
<br>
<br>
<br>

Observe que o modelo com o algoritmo LightGBM apresenta melhores resultados para o AUC e logloss, já para o f1_score pode estar
acontecendo que o ponto de corte de 0.5 não seja o ótimo.

# 4. Pos análise

O pos análise consistiu em analizar as variáveis que tem maior impacto sobre a variável resposta, sent_to_analyses. Assim, apresentamos no
seguinte gráfico a importancia relativas das variáveis.

<div align="center">
    <img width="450" src="imagenes/importancia_lgbm.png" />
    <p>Figura 4.1: Importância relativas das variáveis.</p>
</div>
<br>
<br>
<br>
Podemos observar que as variáveis collateral_debt e collateral_value em escala log são as duas variáveis com um maior peso 
sobre a variável resposta.

- collateral_debt:  Valor que o automovel do cliente tem de dão­vida (ex. Valor que ainda estão financiados)
- collateral_value:  Valor do automovel que serão dado em garantia.
 
Uma suposição seria que quando o valor de um auto estar ainda financiado é alto a probabilidade de ser enviado a entrevista diminue.
 
<div align="center">
    <img width="450" src="imagenes/collateral_boxplot.png" />
    <p>Figura 4.2: Collateral_debt_log boxplot de acordo de uma pessoa foi enviada para o análise.</p>
</div>
<br>
<br>
<br>


A continuação foi avaliado os resíduos para conseguir se o modelo apresenta algúm afastamento espacio temporal assim como
 a presençã de possiveis pontos atípicos.
 
 <div align="center">
    <img width="450" src="imagenes/residuals.png" />
    <p>Figura 4.3: Resíduo do tipo componente de desvio.</p>
</div>
<br>
<br>
<br>

Podemos apreciar que não temos uma autocorrelação ja que os pontos tem um comportamento aleatôrio. Assim, o modelo
indica que apresenta um bom ajuste. Logo, novamente mediante CV, foi otimizado o ponto de corte utilizando a métrica f1 score, 
assim o ponto de corte ótimo foi de 0.283. Por ultimo é apresentado a matriz de confusã para o conjunto de test.


 <div align="center">
    <img width="450" src="imagenes/confusion_matrix.png" />
    <p>Figura 4.4: Matriz de confusão.</p>
</div>
<br>
<br>
<br>